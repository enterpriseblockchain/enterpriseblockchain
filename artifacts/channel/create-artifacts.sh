# set PATH so it includes HLF bin if it exists
if [ -d "/workspaces/HLF-UNINETWORK/fabric-samples/bin" ] ; then
PATH="/workspaces/HLF-UNINETWORK/fabric-samples/bin:$PATH"
fi

chmod -R 0755 ./crypto-config
# Delete existing artifacts
rm -rf ./crypto-config
rm genesis.block mychannel.tx
rm -rf ../../channel-artifacts/*
rm natuni-genesis.block natuni-channel.tx


#Generate Crypto artifacts for organizations
cryptogen generate --config=./crypto-config.yaml --output=./crypto-config/

# Set the path to the configtx.yaml file
export FABRIC_CFG_PATH=/workspaces/enterpriseblockchain/artifacts/channel

# Generate the genesis block for the University Consortium Orderer
configtxgen -profile NatuniOrdererGenesis -channelID ordererchannel -outputBlock natuni-genesis.block


# Create the channel NatuniChannel
configtxgen -outputCreateChannelTx ./natuni-channel.tx -profile NatuniChannel -channelID natunichannel



